package com.cc.model;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class Visit {
    private float latitude;
    private float longitude;
    private Calendar arrivalTime;
    private Calendar departureTime;

    /**
     *
     * @param latitude          the latitude of the visited location (e.g. 45.12345)
     * @param longitude         the longitude of the visited location (e.g. -118.12345)
     * @param arrivalTime       arrival time: when the user arrived at the location (e.g. 5/30/2015 10:12:35)
     * @param departureTime     departure time: when the user departed the location (e.g. 5/30/2015 18:12:35)
     */
    public Visit(final float latitude,
                 final float longitude,
                 final Calendar arrivalTime,
                 final Calendar departureTime) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
    }

    public synchronized long getDurationInMinutes() {

        final Calendar startTime = (Calendar) arrivalTime.clone();
        final Calendar endTime = (Calendar) departureTime.clone();

        // Nothing to do if start date is after end date
        if (startTime.compareTo(endTime) >= 0) {
            return 0;
        }

        if (startTime.get(Calendar.DAY_OF_MONTH) == endTime.get(Calendar.DAY_OF_MONTH)) {

            // If start and end time are out of the visit time range then we return zero
            if (startTime.get(Calendar.HOUR_OF_DAY) >= 8
                    && startTime.get(Calendar.HOUR_OF_DAY) < 20
                    && endTime.get(Calendar.HOUR_OF_DAY) >= 8
                    && endTime.get(Calendar.HOUR_OF_DAY) < 20) {
                return 0;
            }

            // If start and end time are in the same day and start is before 8am and end is
            // in the range of 8 to 20 then we normalize end date to be 8am
            if (startTime.get(Calendar.HOUR_OF_DAY) < 8
                    && endTime.get(Calendar.HOUR_OF_DAY) > 8
                    && endTime.get(Calendar.HOUR_OF_DAY) <= 20) {
                endTime.set(Calendar.HOUR_OF_DAY, 8);
            }
        }

        // If start time is out of the visit range then we normalize it to be the visit time at 8pm
        if (startTime.get(Calendar.HOUR_OF_DAY) < 20 && startTime.get(Calendar.HOUR_OF_DAY) > 8) {
            startTime.set(Calendar.HOUR_OF_DAY, 20);
        }

        // If end time is out of the visit range then we normalize it to be the visit time at 8am
        if (endTime.get(Calendar.HOUR_OF_DAY) > 8 && endTime.get(Calendar.HOUR_OF_DAY) < 20) {
            endTime.set(Calendar.HOUR_OF_DAY, 8);
        }

        final long diffInMs = Math.abs(endTime.getTimeInMillis() - startTime.getTimeInMillis());
        double minutes = TimeUnit.MINUTES.convert(diffInMs, TimeUnit.MILLISECONDS);

        // Calculate how many days we have so we can subtract minutes that are
        // not part of the expected visiting time.
        // We need to subtract half a day always because after normalizing end time
        // we have 12 hours we do not need from the last day
        final double days = (minutes / 60 / 24) - 0.5;
        final double outOfVisitTimeMinutes = TimeUnit.MINUTES.convert((long) days, TimeUnit.DAYS) / 2;

        return (long) (minutes - outOfVisitTimeMinutes);
    }
}