package com.cc.model;

import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;

public class VisitTest {

    @Test
    public void when_dates_equal_8pm_to_8am_range() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,20,0),
                createCalendar(2,8,0));
        assertEquals(720, visit.getDurationInMinutes());
    }

    @Test
    public void when_dates_equal_8pm_to_8am_range_multiple_days() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,20,0),
                createCalendar(3,8,0));
        assertEquals(1440, visit.getDurationInMinutes());
    }

    @Test
    public void when_arrive_after_8pm() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,22,0),
                createCalendar(2,8,0));
        assertEquals(600, visit.getDurationInMinutes());
    }

    @Test
    public void when_arrive_before_8pm() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,17,0),
                createCalendar(2,8,0));
        assertEquals(720, visit.getDurationInMinutes());
    }

    @Test
    public void when_departure_before_8am() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,20,0),
                createCalendar(2,6,0));
        assertEquals(600, visit.getDurationInMinutes());
    }

    @Test
    public void when_departure_after_8am() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,20,0),
                createCalendar(2,10,0));
        assertEquals(720, visit.getDurationInMinutes());
    }

    @Test
    public void when_departure_multiple_days_out_off_visit_time() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,20,0),
                createCalendar(4,8,0));
        assertEquals(2160, visit.getDurationInMinutes());
    }

    @Test
    public void when_arrival_and_departure_same_day() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,22,0),
                createCalendar(1,24,0));
        assertEquals(120, visit.getDurationInMinutes());
    }

    @Test
    public void when_arrival_and_departure_out_of_visit_time() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,17,0),
                createCalendar(1,18,0));
        assertEquals(0, visit.getDurationInMinutes());
    }

    @Test
    public void when_arrival_and_departure_same_day_after_midnight() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,1,0),
                createCalendar(1,3,0));
        assertEquals(120, visit.getDurationInMinutes());
    }

    @Test
    public void when_stay_for_10_minutes_after_20pm() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,20,0),
                createCalendar(1,20,10));
        assertEquals(10, visit.getDurationInMinutes());
    }

    @Test
    public void when_stay_for_10_minutes_at_night() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,22,20),
                createCalendar(1,22,30));
        assertEquals(10, visit.getDurationInMinutes());
    }

    @Test
    public void when_stay_for_10_minutes_after_midnight() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,1,05),
                createCalendar(1,1,15));
        assertEquals(10, visit.getDurationInMinutes());
    }

    @Test
    public void when_stay_for_some_minutes_cross_days() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,23,50),
                createCalendar(2,0,22));
        assertEquals(32, visit.getDurationInMinutes());
    }

    /**
     * Nov 1, 7AM  -  Nov 1, 8PM = 60 minutes (counting minutes from 7AM to 8AM)
     */
    @Test
    public void test60MinutesIn13Hours() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,7,0),
                createCalendar(1,20,0));
        assertEquals(60, visit.getDurationInMinutes());
    }

    /**
     * Nov 1, 7:00AM  -  Nov 1, 8:30PM = 90 minutes (counting minutes from 7AM to 8AM and 8PM to 8:30PM)
     */
    @Test
    public void test90MinutesIn13AndAHalfHours() {
        final Visit visit = new Visit(0,0,
                createCalendar(1,7,0),
                createCalendar(1,20,30));
        assertEquals(90, visit.getDurationInMinutes());
    }

    private Calendar createCalendar(final int dayOfMonth,
                                    int hourOfDay,
                                    final int minute) {
        final Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        cal.set(Calendar.MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }
}